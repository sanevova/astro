/**
 * File: SCingletonFactory.cpp
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 17.09.12 at 22:53:04.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SINGLETON_FACTORY_IMPLEMENTATION
#define _SINGLETON_FACTORY_IMPLEMENTATION

#include "CSingletonFactory.h"
#include "CMassiveObject.h"

CSingletonFactory* CSingletonFactory::_instance = NULL;

CSingletonFactory* CSingletonFactory::getInstance() {
    if (_instance == NULL)
        _instance = new CSingletonFactory();
    return _instance;
}

CSingletonFactory::CSingletonFactory() {
}

CMassiveObject* CSingletonFactory::CreatePlanet() const {
    return new CMassiveObject(cImgPlanet);
}

CMassiveObject* CSingletonFactory::CreateStar() const {
    return new CMassiveObject(cImgSun, cDefaultBigMass, cMiddlePos, cZeroVectorT);
}

CMassiveObject* CSingletonFactory::CreateSatellite() const {
    return new CMassiveObject(cImgSatellite, cDefaultSatelliteMass, cMiddlePos, cZeroVectorT);
}

#endif