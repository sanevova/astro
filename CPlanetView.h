/**
 * File: CPlanetView.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 02.10.12 at 14:21:45.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _PLANET_VIEW_HEADERS
#define _PLANET_VIEW_HEADERS

#include "ConstDeclarations.h"

class CMassiveObject;

class CPlanetView {
    private:
        point_t mPos;
        bool mClickedFlag;
        ALLEGRO_BITMAP* mBitmap;
        double mAngle;

    public:
    //  Constructors
        CPlanetView();
      //CPlanetView(CPlanetDataContract*);

    //  Destructors
        ~CPlanetView();

    //  Getters
        point_t getPos() const;
        bool isMouseDown() const;
        ALLEGRO_BITMAP* getBitmap() const;
        double getAngle() const;

    //  Setters
        void setPos(point_t);
        void setClickedFlag(bool);
        void setBitmap(ALLEGRO_BITMAP*);
        void setAngle(double);

        void selfDraw(ALLEGRO_BITMAP*) const;
};

#endif