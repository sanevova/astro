/**
 * File: CSingletonFactory.h
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 17.09.12 at 22:53:23.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SINGLETON_FACTORY_HEADERS
#define _SINGLETON_FACTORY_HEADERS

#include "ConstDeclarations.h"

class CMAssiveObject;

class CSingletonFactory {
    friend class CMassiveObject;
    private:
        static CSingletonFactory* _instance;
    protected:
        CSingletonFactory();
    public:
        static CSingletonFactory* getInstance();

//      Creating objects with zero speed and position that corresponds to the middle of the screen.
        virtual CMassiveObject* CreatePlanet() const;
        virtual CMassiveObject* CreateStar() const;
        virtual CMassiveObject* CreateSatellite() const;
};

#endif