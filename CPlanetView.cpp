/**
 * File: CPlanetView.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 02.10.12 at 14:21:52.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _PLANET_VIEW_IMPLEMENTATION
#define _PLANET_VIEW_IMPLEMENTATION

#include "CPlanetView.h"
#include "CMassiveObject.h"

//  Constructors
CPlanetView::CPlanetView() :
    mPos(cMiddlePos),
    mBitmap(NULL),
    mClickedFlag(false),
    mAngle(0.0) {
}


//  Destructors
CPlanetView::~CPlanetView() {
}

//  Getters
point_t CPlanetView::getPos() const {
    return this->mPos;
}

ALLEGRO_BITMAP* CPlanetView::getBitmap() const {
    return this->mBitmap;
}

bool CPlanetView::isMouseDown() const {
    return this->mClickedFlag;
}

double CPlanetView::getAngle() const {
    return this->mAngle;
}

//  Setters
void CPlanetView::setPos(point_t the_point) {
    this->mPos = the_point;
}

void CPlanetView::setClickedFlag(bool newFlag) {
    this->mClickedFlag = newFlag;
}

void CPlanetView::setBitmap(ALLEGRO_BITMAP* newBitmap) {
    this->mBitmap = newBitmap;   
}

void CPlanetView::setAngle(double newAngle) {
    this->mAngle = newAngle;    
}

#endif