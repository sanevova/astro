/**
 * File: CMassiveObject.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 07.09.12 at 21:28:29.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _MASSIVE_OBJECT_IMPLEMENTATION
#define _MASSIVE_OBJECT_IMPLEMENTATION

#include "CMassiveObject.h"

#include "CAllegroEngine.h"
#include "CMemoryPrinter.h"
#include "CTargetBitmapSwitcher.h"

//  Constructors
CMassiveObject::CMassiveObject() {
    this->internalInit();
    this->loadBitmap();
}

CMassiveObject::CMassiveObject(string sFileName) {
    this->internalInit();
    this->mImage = sFileName;
    this->loadBitmap();
}

CMassiveObject::CMassiveObject(string sFileName, mass_t mass, point_t pos, vector_t speed) {
    this->internalInit();
    this->mPos = pos;
    this->mMass = mass;
    this->mSpeed = speed;
    this->mImage = sFileName;
    this->loadBitmap();
}


CMassiveObject::CMassiveObject(const CMassiveObject &obj) :
    mMass(obj.getMass()),
    mPos(obj.getPos()),
    mSpeed(obj.getSpeed()),
    mAcceleration(obj.getAcceleration()),
    mForce(obj.getForce()),
    mImage(obj.getImage()),
    mClickedFlag(obj.isMouseDown()),
    mLinearScale(obj.getLinearScale()),
    mAngle(obj.getAngle()),
    mTopology(obj.getTopology()) {
        this->loadBitmap();
}


//  Destructors
CMassiveObject::~CMassiveObject() {
//  al_destroy_bitmap(this->mBitmap);
}


//  Getters
mass_t CMassiveObject::getMass() const {
    return this->mMass;
}

point_t CMassiveObject::getPos() const {
    return this->mPos;
}

CVector CMassiveObject::getSpeed() const {
    return CVector(this->mSpeed);
}

CVector CMassiveObject::getAcceleration() const {
    return CVector(this->mAcceleration);
}

CVector CMassiveObject::getForce() const {
    return CVector(this->mForce);
}

string CMassiveObject::getImage() const {
    return this->mImage;
}

ALLEGRO_BITMAP* CMassiveObject::getBitmap() const {
    return this->mBitmap;
}

coord_t CMassiveObject::getBitmapRadius() const {
    return this->mBitmapRadius;
}

double CMassiveObject::getLinearScale() const {
    return this->mLinearScale;
}

double CMassiveObject::getAngle() const {
    return this->mAngle;
}

topology_t CMassiveObject::getTopology() const {
    return this->mTopology;   
}

bool CMassiveObject::isMouseDown() const {
    return this->mClickedFlag;
}

//  Setters
void CMassiveObject::setForce(CVector the_force) {
    this->mForce = *(new CVector(the_force));
}

void CMassiveObject::setMass(mass_t the_mass) {
    this->mMass = the_mass;
}

void CMassiveObject::setPos(point_t the_point) {
    this->mPos = the_point;
}

void CMassiveObject::setSpeed(CVector the_speed) {
    this->mSpeed = *(new CVector(the_speed));
}

void CMassiveObject::setAcceleration(CVector the_acceleration) {
    this->mAcceleration = *(new CVector(the_acceleration));
}

void CMassiveObject::setTopology(topology_t newTopology) {
    this->mTopology = newTopology;
}

void CMassiveObject::setClickedFlag(bool newFlag) {
    this->mClickedFlag = newFlag;
}

void CMassiveObject::setBitmap(ALLEGRO_BITMAP* newBmp) {
    this->mBitmap = newBmp;
}

//  Methotds
//  Public
CVector CMassiveObject::getDirectionTo(CMassiveObject* obj) const {
    CVector realDirection(make_pair(this->getDistanceXAxisTo(obj), this->getDistanceYAxisTo(obj)));
    if (realDirection.getValue() < 2)
       realDirection.toLength(2.0);
    return   realDirection;
}

CVector CMassiveObject::getForceBetween(CMassiveObject* obj) const {
    CVector direction = this->getDirectionTo(obj);
    //  F = G (mM / R^2)  - Gravity law
    coord_t force_power = cGammaGravityConstant * this->getMass() * obj->getMass() / sqr(this->getDirectionTo(obj).getValue());
    direction.toLength(force_power);
    return direction;
}

void CMassiveObject::reverseSpeed() {
    coord_t newXAxisDirection = -1.0 * this->mSpeed.getX();
    coord_t newYAxisDirection = -1.0 * this->mSpeed.getY();
    this->mSpeed = CVector(newXAxisDirection, newYAxisDirection);
}

coord_t CMassiveObject::getDistanceTo(CMassiveObject* obj) const {
    return sqrt(sqr(this->mPos.X - obj->getPos().X) + sqr(this->mPos.Y - obj->getPos().Y));
}

bool CMassiveObject::collidesWith(CMassiveObject* obj) const {
    //  Consider both objects are round shaped.
    //  CMemoryPrinter printing = *(new CMemoryPrinter());
    return abs(this->getDistanceTo(obj)) <= this->getBitmapRadius() + obj->getBitmapRadius();
}

bool CMassiveObject::containsPoint(coord_t x, coord_t y) const {
    return sqrt(sqr(this->mPos.X - x) + sqr(this->mPos.Y - y)) <= this->mBitmapRadius;
}

void CMassiveObject::throwClockWiseCircleAround(CMassiveObject* obj) {
    //  before(x, y) -> after(y, -x), so that (before, after) = 0. i.e. vectors 'before' and 'after' are ortogonal.
    //  'before' vector corresponds to the direction (this) --> (obj)
    //  'after' vector corresponds to the newSpeed vector
    //  ['before' x 'after'] = -(x^2 + y^2) < 0, therefore this is clockwise thing.
    coord_t speedValue = this->calculateCircleSpeedAround(obj);
    CVector direction = this->getDirectionTo(obj);
    CVector newSpeed = CVector(direction.getY(), -1.0 * direction.getX());
    newSpeed.toLength(speedValue);
    this->mSpeed = newSpeed;
}

void CMassiveObject::throwCounterClockWiseCircleAround(CMassiveObject* obj) {
    //  (x, y) -> (-y, x)
    //  ['before' x 'after'] = x^2 + y^2 > 0, therefore this is counterclockwise thing.
    coord_t speedValue = this->calculateCircleSpeedAround(obj);
    CVector direction = this->getDirectionTo(obj);
    CVector newSpeed = CVector(-1.0 * direction.getY(), direction.getX());
    newSpeed.toLength(speedValue);
    this->mSpeed = newSpeed;
}

void CMassiveObject::scaleAndRotate(double linearScale, double angle) {
    this->mLinearScale *= linearScale;
    this->mAngle += angle;

    double angleInRadians = angle / 180 * Pi;                                       //  Converting to Radians
    coord_t newWidth = linearScale * al_get_bitmap_width(this->mBitmap) + 10;       //  Magic number of pixels LoL
    coord_t newHeight = linearScale * al_get_bitmap_height(this->mBitmap) + 10;
    ALLEGRO_BITMAP* retBitmap = al_create_bitmap(newWidth, newHeight);
    CTargetBitmapSwitcher switcher(retBitmap);
    al_draw_scaled_rotated_bitmap(this->mBitmap, al_get_bitmap_width(this->mBitmap)/2, al_get_bitmap_height(this->mBitmap)/2,
                                    newWidth/2,
                                    newHeight/2,
                                    linearScale, linearScale, angleInRadians, 0);
    al_destroy_bitmap(this->mBitmap);
    this->mBitmap = retBitmap;
    CAllegroEngine::getInstaice()->setSpriteByName(mImage, retBitmap);
    this->mBitmapRadius = newWidth / 2;
//  cerr << al_get_bitmap_width(retBitmap) << "ret\n";
}

void CMassiveObject::performClick() {
    this->mClickedFlag = true;
}

void CMassiveObject::releaseClick() {
    this->mClickedFlag = false;
}

void CMassiveObject::draw() const {
    drawCentre(this->mBitmap, this->mPos.X, this->mPos.Y);
/*    al_draw_bitmap(this->mBitmap, this->mPos.X - al_get_bitmap_width(this->mBitmap) / 2,
                                  this->mPos.Y - al_get_bitmap_height(this->mBitmap) / 2, 0);*/
}

void CMassiveObject::move() {
    this->updAcceleration();
    this->updSpeed();
    this->normalizeSpeed();
    this->updPos();

    switch (this->mTopology) {
    case TOPOLOGY_TYPE_THOR:
        this->updateThorLikeCoordinates();
        break;
    case TOPOLOGY_TYPE_REFL:
        this->reflectSpeedAgainsScreenBorders();
        break;
    }
//    this->draw();
}


//  Private
void CMassiveObject::internalInit() { 
    this->mPos = cMiddlePos;
    this->mMass = cDefaultSmallMass;
    this->mSpeed = cDefaultSpeed;
    this->mAcceleration = cDefaultAcceleration;
    this->mImage = cImgDefaultObject;
    this->mClickedFlag = false;
    this->mLinearScale = 1.0;
    this->mAngle = 0.0;
    this->mTopology = cDefaultTopologyType;
    this->mForce = *(new CVector(cZeroVectorT));
}

void CMassiveObject::loadBitmap() {
    mBitmap = CAllegroEngine::getInstaice()->getSpriteByName(mImage);
/*    this->mBitmap = al_load_bitmap(getImagePath(this->mImage));
    assert(this->mBitmap != NULL);
    this->mBitmapRadius = al_get_bitmap_width(this->mBitmap) / 2;
*/
}

void CMassiveObject::updAcceleration() {
    this->mAcceleration = (this->mForce.withLength(this->mForce.getValue() / this->mMass));
}

void CMassiveObject::updSpeed() {
    this->mSpeed.add(this->mAcceleration.withLength(this->mAcceleration.getValue() * deltaT));
}

void CMassiveObject::updPos() {
    this->mPos.X += this->mSpeed.getX() * deltaT;
    this->mPos.Y += this->mSpeed.getY() * deltaT;
}

void CMassiveObject::normalizeSpeed() {
    while (this->mSpeed.getValue() > cSpeedLimit)
        this->mSpeed.toLength(cSpeedDecreasingFactor * this->mSpeed.getValue());
//       this->mSpeed = satellite->getSpeed().withLength(0.3 * satellite->getSpeed().getValue()));
}

coord_t CMassiveObject::getDistanceXAxisTo(CMassiveObject* obj) const {
//  return min( abs(a.X - b.X), min( abs(a.X) + abs(DISPLAY_WIDTH - b.X), abs(b.X) + abs(DISPLAY_WIDTH - a.X)) );
    point_t objPos = obj->getPos();
    point_t pos = this->mPos;
    coord_t a = objPos.X - pos.X;
    return a;

//  Thor topology expansion
    coord_t b = (cDisplayWidth - objPos.X) + pos.X;
    coord_t c = objPos.X + abs(cDisplayWidth - pos.X);
//  a = max(a, 0), b = max(a, 0), c = max(a, 0);
    if (abs(a) < min(abs(b), abs(c)))
        return a;
    else
    if (abs(b) < min(abs(a), abs(c)))
        return b;
    else
    if (abs(c) < min(abs(a), abs(b)))
        return c;

    return 0;
}

coord_t CMassiveObject::getDistanceYAxisTo(CMassiveObject* obj) const {

    point_t objPos = obj->getPos();
    point_t pos = this->mPos;
    coord_t a = objPos.Y - pos.Y;
    return a;

//  Thor topology expansion
    coord_t b = (cDisplayHeight - objPos.Y) + pos.Y;
    coord_t c = objPos.Y + abs(cDisplayHeight - pos.Y);
    if (abs(a) < min(abs(b), abs(c)))
        return a;
    else
    if (abs(b) < min(abs(a), abs(c)))
        return b;
    else
    if (abs(c) < min(abs(a), abs(b)))
        return c;

    return 0;
}

coord_t CMassiveObject::calculateCircleSpeedAround(CMassiveObject* someObj) const {
    return sqrt(cGammaGravityConstant * someObj->getMass() / this->getDirectionTo(someObj).getValue());
}

void CMassiveObject::updateThorLikeCoordinates() {
    if (this->mPos.X < -cBordedExceedingLimit)
        this->mPos.X = cDisplayWidth - cBordedExceedingLimit;
    else if (this->mPos.X > cDisplayWidth + cBordedExceedingLimit)
        this->mPos.X = cBordedExceedingLimit;

    if (this->mPos.Y < -cBordedExceedingLimit)
        this->mPos.Y = cDisplayHeight - cBordedExceedingLimit;
    else if (this->mPos.Y > cDisplayHeight + cBordedExceedingLimit)
        this->mPos.Y = cBordedExceedingLimit;
}


void CMassiveObject::reflectSpeedAgainsScreenBorders() {
    coord_t speedByX = this->mSpeed.getX();// * (1.0 - sqrt(cEnergyLossFactor));
    coord_t speedByY = this->mSpeed.getY();// * (1.0 - sqrt(cEnergyLossFactor));
    bool wallAppeared = false;
    if (this->mPos.X + cBordedExceedingLimit <= this->mBitmapRadius ||
        cDisplayWidth - this->mPos.X + cBordedExceedingLimit <= this->mBitmapRadius) {
            wallAppeared = true;
            speedByX = -speedByX;
        }
    if (this->mPos.Y + cBordedExceedingLimit <= this->mBitmapRadius ||
        cDisplayHeight - this->mPos.Y + cBordedExceedingLimit <= this->mBitmapRadius) {
            wallAppeared = true;
            speedByY = -speedByY;
    }

    CVector newSpeed = CVector(speedByX, speedByY);
    if (wallAppeared) {
        newSpeed.toLength(newSpeed.getValue() * (1.0 - sqrt(cEnergyLossFactor)));
    }
    this->mSpeed = newSpeed;
}

#endif