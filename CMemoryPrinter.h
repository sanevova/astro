/**
 * File: CMemoryPrinter.h
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 17.09.12 at 20:45:30.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _MEMORY_PRINTER_IMPLEMENTATION
#define _MEMORY_PRINTER_IMPLEMENTATION

#include "ConstDeclarations.h"

class CMemoryPrinter {
    private:
        size_t mStartMemoryUsage;
    public:
        CMemoryPrinter() : mStartMemoryUsage(GetAppMemUsage()) {
//          cerr << "PrinterObject created.\n";
        }
        ~CMemoryPrinter() {
//          cerr << this->mStartMemoryUsage << "\t ->\t\t";
            cerr << "Added " << GetAppMemUsage() - this->mStartMemoryUsage << " more bytes of memory.\t->\t";
            cerr << GetAppMemUsage() << " now.\n";
        }
};    

#endif
