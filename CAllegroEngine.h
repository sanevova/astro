/**
 * File: CAllegroEngine.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 09.09.12 at 11:27:17.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _ENGINE_HEADERS
#define _ENGINE_HEADERS

#include "ConstDeclarations.h"

class CAllegroEngine {
    private:
        static CAllegroEngine* _instance;

        HWND mProcessForegroundWindow;

        ALLEGRO_DISPLAY* mDisplay;
        ALLEGRO_EVENT_QUEUE* mEvent_queue;
        ALLEGRO_TIMER* mTimer;
        bool mRedraw;

        vector <ALLEGRO_BITMAP*> mSprites;
        vector <string> mNames;

        void loadResources();
    protected:
        CAllegroEngine();

    public:
        static CAllegroEngine* getInstaice();

    //  Destructors
        ~CAllegroEngine();
    
    //  Getters
        ALLEGRO_DISPLAY* getDisplay() const;
        ALLEGRO_EVENT_QUEUE* getEventQueue() const;
        ALLEGRO_TIMER* getTimer() const;

        HWND getProcessForegroundWindow() const ;

    //  Setters
        void setDisplay(ALLEGRO_DISPLAY*);

    //  Methods
        void loadEngine();
        void destroyEngine();

        ALLEGRO_BITMAP* getSpriteById(int);
        ALLEGRO_BITMAP* getSpriteByName(string);
        void setSpriteByName(string, ALLEGRO_BITMAP*);

};

#endif