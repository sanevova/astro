/**
 * File: CWordlController.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 10.10.12 at 20:41:26.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _WORLD_CONTROLLER_HEADERS
#define _WORLD_CONTROLLER_HEADERS

#include "ConstDeclarations.h"

class CSceneController;

class CWorldController {
    private:
    //  Fields
        CSceneController* mController;

        bool mMainLoopContinue;
        bool mRedraw;
        clock_t mRenderingTime;
        int mIterationCount;

        ALLEGRO_EVENT mLastEvent;
        ALLEGRO_FONT* mTextFont;

        int mClickedIndex;
        point_t mDragFrom;
        point_t mDragTo;
    //  Methods
        void breakMainLoop();
        bool checkForErrors();

    //  Event Delivery!
        void onTimerEvent();
        void onKeyDownEvent();
        void onMouseDownEvent();
        void onMouseUpEvent();
        void onMouseMoveEvent();
    public:
    //  Constructors
        CWorldController();

    //  Destructors
        ~CWorldController();

    //  Methods
        void launch();
};

#endif