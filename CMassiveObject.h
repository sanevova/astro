/**
 * File: CMassiveObject.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 07.09.12 at 21:28:45.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _MASSIVE_OBJECT_HEADERS
#define _MASSIVE_OBJECT_HEADERS

#include "ConstDeclarations.h"
#include "CVector.h"

class CMassiveObject {
    friend class CVector;
    private:
    //  Fields
    //  Physical parameters
        mass_t mMass;
        point_t mPos;
        CVector mSpeed;
        CVector mAcceleration;
        CVector mForce;

    //  View parameters
        string mImage;
        ALLEGRO_BITMAP* mBitmap;

        coord_t mBitmapRadius;          //  Consider the object as round shaped.
        double mLinearScale;
        double mAngle;
        topology_t mTopology;
        bool mClickedFlag;

    //  Methods
        void internalInit();
        void loadBitmap();

        void updAcceleration();
        void updSpeed();
        void updPos();
        void normalizeSpeed();

        void updateThorLikeCoordinates();
        void reflectSpeedAgainsScreenBorders();

        coord_t getDistanceXAxisTo(CMassiveObject*) const;
        coord_t getDistanceYAxisTo(CMassiveObject*) const;

        coord_t calculateCircleSpeedAround(CMassiveObject*) const;
    public:
    //  Constructors
        CMassiveObject();
        CMassiveObject(string);
        CMassiveObject(string,  mass_t, point_t, vector_t);
        CMassiveObject(const CMassiveObject&);
//        CMassiveObject(CPlanetDataContract*);

    //  Destructors
        ~CMassiveObject();

    //  Getters
        mass_t getMass() const;
        point_t getPos() const;
        CVector getSpeed() const;
        CVector getAcceleration() const;
        CVector getForce() const;
        string getImage() const;

        ALLEGRO_BITMAP* getBitmap() const;
        coord_t getBitmapRadius() const;
        double getLinearScale() const;
        double getAngle() const;
        topology_t getTopology() const;

        bool isMouseDown() const;

    //  Setters
        void setMass(mass_t);
        void setPos(point_t);
        void setSpeed(CVector);
        void setAcceleration(CVector);
        void setForce(CVector);
        void setTopology(topology_t);
        void setClickedFlag(bool);

        void setBitmap(ALLEGRO_BITMAP*);
    //  Methods
        CVector getDirectionTo(CMassiveObject*) const;  //  Returns vecor from (this) to (obj)
        CVector getForceBetween(CMassiveObject*) const;

        coord_t getDistanceTo(CMassiveObject*) const;
        bool collidesWith(CMassiveObject*) const;
        bool containsPoint(coord_t, coord_t) const;

        void reverseSpeed();
        void throwClockWiseCircleAround(CMassiveObject*);
        void throwCounterClockWiseCircleAround(CMassiveObject*);

        void scaleAndRotate(double linearScale, double angle);

        void performClick();
        void releaseClick();

        void move();
        void draw() const;

};

#endif