/**
 * File: CWordlController.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 10.10.12 at 20:41:33.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _WORLD_CONTROLLER_IMPLEMENTATION
#define _WORLD_CONTROLLER_IMPLEMENTATION

#include "ConstDeclarations.h"
#include "CAllegroEngine.h"
#include "CSceneModel.h"
#include "CSceneView.h"
#include "CSceneController.h"
#include "CWorldController.h"
#include "CMassiveObject.h"
#include "CSingletonFactory.h"
#include "CVector.h"
#include "CTargetBitmapSwitcher.h"

//  Constructors
CWorldController::CWorldController() :
    mController(NULL),
    mMainLoopContinue(true),
    mRedraw(true),
    mLastEvent(ALLEGRO_EVENT()),
    mRenderingTime((clock_t) 0),
    mIterationCount(0),
    mTextFont(NULL),
    mClickedIndex(CLICKED_FLAG_NONE),
    mDragFrom(),
    mDragTo() {

    CAllegroEngine::getInstaice()->loadEngine();

    this->mTextFont= al_load_font(getImagePath(cFontToribash), cFontSize, 0);
    al_clear_to_color(al_map_rgb(0xff, 0xff, 0xff));
    al_draw_text(this->mTextFont, al_map_rgb(0x00, 0x00, 0x00), cMiddleX, cMiddleY, ALLEGRO_ALIGN_CENTRE, "Loading...");
    al_flip_display();
}

//  Destructors
CWorldController::~CWorldController() {
    CAllegroEngine::getInstaice()->destroyEngine();
}

//  Methods
//  Public
void CWorldController::launch() {
    CSceneModel* model = new CSceneModel();
    CSceneView* view = new CSceneView(al_get_backbuffer(CAllegroEngine::getInstaice()->getDisplay()));
    model->createFirstScene(CSingletonFactory::getInstance());
    this->mController = new CSceneController(model, view);
//  al_hide_mouse_cursor(CAllegroEngine::getInstaice()->getDisplay());
    while (this->mMainLoopContinue) {
        clock_t launchStarted = clock();
        bool hasNextEvent = al_get_next_event(CAllegroEngine::getInstaice()->getEventQueue(), &this->mLastEvent);
        switch(this->mLastEvent.type) {
            case ALLEGRO_EVENT_TIMER:
                if (this->mRedraw)
                    this->onTimerEvent();
                break;

            case ALLEGRO_EVENT_KEY_DOWN:
                this->onKeyDownEvent();
                break;

            case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                this->onMouseDownEvent();
                break;

            case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                this->onMouseUpEvent();
                break;

            case ALLEGRO_EVENT_MOUSE_AXES:
                this->onMouseMoveEvent();
                break;

            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                this->breakMainLoop();
                break;
        }

        this->mRenderingTime = clock() - launchStarted;
        if (this->checkForErrors())
            this->breakMainLoop();
    }
    delete model;
    delete view;
}

//  Private
void CWorldController::breakMainLoop() {
    this->mMainLoopContinue = false;
}

bool CWorldController::checkForErrors() {
    bool errorOccured = false;
    if (GetAppMemUsage() / 1024 / 1024 > cMemoryLimit) {
        cerr << "MemoryLimit exceeded: " << GetAppMemUsage() << "\n";
        bool clicked = al_show_native_message_box(
            CAllegroEngine::getInstaice()->getDisplay(),
            "Exceeding Memory Limit",
            "Error oruced.",
            parseToCharArray("The application has exceeded allowed memory limit - " + 
                                parseToString(cMemoryLimit).substr(0, parseToString(cMemoryLimit).size()-1 - cShiftDecimalPoint) +
                                " MBytes."),
            NULL,
            ALLEGRO_MESSAGEBOX_ERROR
        );
        errorOccured = true;
    }

    if (this->mRenderingTime > cRenderingTimeLimit && this->mRedraw &&
        CAllegroEngine::getInstaice()->getProcessForegroundWindow() == GetForegroundWindow()) {

        cerr << "RenderingTimeLimit exceeded. It\'s over " << cRenderingTimeLimit << " milliseconds!\n"
             << this->mRenderingTime << " ms is way too much.\n"
             << (this->mIterationCount - 1) / FPS + 1<< "\'s second.\n";
        bool clicked = al_show_native_message_box(
            CAllegroEngine::getInstaice()->getDisplay(),
            "Porcess terminated.",
            "RenderingTimeLimit exceeded.",
            "The process is running too slow. We most certainly will work on this.",
            NULL,
            ALLEGRO_MESSAGEBOX_WARN
        );
        errorOccured = true;
    }

    return errorOccured;
}

void CWorldController::onTimerEvent() {
    ++this->mIterationCount;
    this->mController->performLoopStep();
}

void CWorldController::onKeyDownEvent() {
    switch (this->mLastEvent.keyboard.keycode) {
        case ALLEGRO_KEY_P:
            this->mRedraw ^= 1;
            cerr << "p\n";
            break;
        case ALLEGRO_KEY_Q: case ALLEGRO_KEY_ESCAPE:
            breakMainLoop();
            break;
    }
}

void CWorldController::onMouseDownEvent() {
    switch(this->mLastEvent.mouse.button) {
        case 1:
            int n = this->mController->getModel()->getListSize();
            bool clickMatches = false;
            for (int i = n - 1; i >= 0 && !clickMatches; --i)
                if (this->mController->getModel()->getElementAt(i)->containsPoint(this->mLastEvent.mouse.x, this->mLastEvent.mouse.y)) {
                    clickMatches = true;
                    this->mController->getModel()->getElementAt(i)->performClick();
                    this->mDragFrom = make_pair(this->mLastEvent.mouse.x, this->mLastEvent.mouse.y);
                    this->mClickedIndex = i;
                }
            this->mRedraw = false;
            break;
    }
}

void CWorldController::onMouseUpEvent() {
    if (this->mLastEvent.mouse.button == 1 && this->mClickedIndex >= 0) {
        cerr << "mouse button released.\n";
        this->mDragTo = make_pair(this->mLastEvent.mouse.x, this->mLastEvent.mouse.y);
        CVector newSpeed(make_pair(this->mDragTo.X - this->mDragFrom.X, this->mDragTo.Y - this->mDragFrom.Y));
        newSpeed.toLength(newSpeed.getValue());
        this->mController->getModel()->getElementAt(this->mClickedIndex)->setSpeed(newSpeed);
        this->mController->getModel()->resetPrevSpeedAt(this->mClickedIndex);
        cerr << this->mDragFrom.X << "-" << this->mDragFrom.Y << "\n";
        cerr << this->mDragTo.X << "-" << this->mDragTo.Y << "\n";
        this->mClickedIndex = CLICKED_FLAG_NONE;
    }
    this->mRedraw = true;
}

void CWorldController::onMouseMoveEvent() {
    this->mController->getView()->updateMousePos(make_pair(this->mLastEvent.mouse.x, this->mLastEvent.mouse.y));
    CTargetBitmapSwitcher switcher(al_get_backbuffer(CAllegroEngine::getInstaice()->getDisplay()));
    if (this->mClickedIndex >= 0) {
        this->mController->getView()->render();

        coord_t x1 = this->mLastEvent.mouse.x;
        coord_t y1 = this->mLastEvent.mouse.y;
        coord_t x2 = this->mController->getModel()->getElementAt(this->mClickedIndex)->getPos().X;
        coord_t y2 = this->mController->getModel()->getElementAt(this->mClickedIndex)->getPos().Y;

        al_draw_line(this->mDragFrom.X, this->mDragFrom.Y, x1, y1, al_map_rgba_f(0.2, 0.8, 0.8, 0.3), 10);
//      this->mController->getView()->drawCursor();
        al_flip_display();
    }
}

#endif