/**
 * File: CVector.cpp
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 08.09.12 at 2:55:18.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _VECTOR_IMPLEMENTATION
#define _VECTOR_IMPLEMENTATION

#include "ConstDeclarations.h"
#include "CVector.h"

//  Constructors
CVector::CVector() : mVector(cZeroVectorT) {
//  this->mVector = cZeroVector;
    this->updValue();
}
                            
CVector::CVector(const CVector &the_vector) : mVector(make_pair(the_vector.getX(), the_vector.getY())) {
    this->updValue();
}

CVector::CVector(coord_t x, coord_t y) : mVector(make_pair(x, y)) {
//  this->mVector = make_pair(x, y);
    this->updValue();
}

CVector::CVector(vector_t direction) : mVector(direction) {
    this->updValue();
}


//  Getters
coord_t CVector::getX() const {
    return this->mVector.X;
}

coord_t CVector::getY() const {
    return this->mVector.Y;
}

force_t CVector::getValue() const {
    return this->mValue;
}

//  Setters
void CVector::setX(coord_t newX) {
    mVector.X = newX;
    this->updValue();
}

void CVector::setY(coord_t newY) {
    mVector.Y = newY;
    this->updValue();
}

//  Methods
//  Private
void CVector::updValue() {
    this->mValue = sqrt(sqr(this->mVector.X) + sqr(this->mVector.Y));
}

//  Public
void CVector::add(CVector the_force) {
    this->mVector.X += the_force.getX();
    this->mVector.Y += the_force.getY();
    this->updValue();
}

CVector CVector::sum(CVector the_force) const {
    CVector retVector(this->mVector);
    retVector.add(the_force);
    return retVector;
}

void CVector::toLength(coord_t len) {
    if (this->getValue() > eps) {
        this->mVector.X *= len / this->getValue();
        this->mVector.Y *= len / this->getValue();
        this->updValue();
    }
    else {
        this->mVector.X = 0;//sqrt(2.0) * len;
        this->mVector.Y = 0;//sqrt(2.0) * len;
        this->updValue();
    }
}

CVector CVector::withLength(coord_t len) const {
    CVector retVector(this->mVector);
    retVector.toLength(len);
    return retVector;
/*  vector_t temp = make_pair(this->getX(), this->getY());
    temp.X *= len / this->getValue();
    temp.Y *= len / this->getValue();
    CVector retVector(temp);
    return retVector;
*/
}

#endif