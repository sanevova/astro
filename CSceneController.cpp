/**
 * File: CSceneController.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 21.09.12 at 19:38:32.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_CONTROLLER_IMPLEMENTATION
#define _SCENE_CONTROLLER_IMPLEMENTATION

#include "CSceneController.h"

#include "CAllegroEngine.h"
#include "CSceneModel.h"
#include "CSceneView.h"
#include "CPlanetDataContract.h"


//  Constructors
CSceneController::CSceneController() : 
    mModel(NULL),
    mView(NULL),
    mIterationsPerStep(cDefaltIteraionCount),
    mMainLoopContinue(true) {
}

//  Destructors
CSceneController::CSceneController(CSceneModel* pModel, CSceneView* pView) :
    mModel(pModel),
    mView(pView),
    mIterationsPerStep(cDefaltIteraionCount),
    mMainLoopContinue(true) {
        int n = this->mModel->getListSize();
        for (int i = 0; i < n; ++i)
            this->mDataCash.push_back(new CPlanetDataContract(this->mModel->getElementAt(i)));
        this->selfUpdateFromModel();
}

//  Getters
ALLEGRO_EVENT CSceneController::getCurrentEvent() const {
    return this->mEventHandler;
}

CSceneModel* CSceneController::getModel() const {
    return this->mModel;
}

CSceneView* CSceneController::getView() const {
    return this->mView;
}

int CSceneController::getIterationsPerStep() const {
    return this->mIterationsPerStep;
}

void CSceneController::setIterationsPerStep(int newIt) {
    this->mIterationsPerStep = newIt;
}

//  Setters
void CSceneController::setCurrentEvent(ALLEGRO_EVENT toSet) {
    this->mEventHandler = toSet;
}

//  Methods
//  Public
void CSceneController::performLoopStep() {
    for (int i = 0; i < this->mIterationsPerStep; ++i)
        this->mModel->performStep();
    this->selfUpdateFromModel();
    this->mView->onUpdateList(this->mDataCash);
    this->mView->render();

    al_flip_display();
}

//  Private
bool CSceneController::updEvent() {
    bool hasNextEvent = al_get_next_event(CAllegroEngine::getInstaice()->getEventQueue(), &this->mEventHandler);
    return hasNextEvent;
}

void CSceneController::modifyModel() {
    this->mModel->performStep();
}

void CSceneController::passToDrawer(CMassiveObject* obj) {
    this->mView->drawOne(obj);
}

void CSceneController::selfUpdateFromModel() {
    assert(this->mDataCash.size() == this->mModel->getListSize());
    int i = 0;
    for (vector<CPlanetDataContract*>::iterator it = mDataCash.begin(); it != mDataCash.end(); ++it, ++i) {
        delete *it;
        *it = new CPlanetDataContract(this->mModel->getElementAt(i));
    }
}


#endif