/** 
 * File: CAllegroEngine.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 09.09.12 at 11:27:00.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _ENGINE_IMPLEMENTATION
#define _ENGINE_IMPLEMENTATION

#include "CAllegroEngine.h"

CAllegroEngine* CAllegroEngine::_instance = NULL;

CAllegroEngine* CAllegroEngine::getInstaice() {
    if (_instance == NULL)
        _instance = new CAllegroEngine();
    return _instance;
}

//  Constructors
CAllegroEngine::CAllegroEngine() : mDisplay(NULL), mEvent_queue(NULL), mTimer(NULL), mRedraw(true) {
    this->mProcessForegroundWindow = GetForegroundWindow();
    bool loadSucceded = true;
    loadSucceded &= al_init() &
                    al_init_image_addon() &
                    al_init_primitives_addon();
    al_init_font_addon();
    loadSucceded &= al_init_ttf_addon() &
                    al_install_mouse() &
                    al_install_keyboard();
    assert(loadSucceded);
}

//  Destructors
CAllegroEngine::~CAllegroEngine() {
    al_destroy_display(this->mDisplay);
    al_destroy_event_queue(this->mEvent_queue);
    al_destroy_timer(this->mTimer);
}

//  Getters
ALLEGRO_DISPLAY* CAllegroEngine::getDisplay() const {
    return this->mDisplay;
}

ALLEGRO_EVENT_QUEUE* CAllegroEngine::getEventQueue() const {
    return this->mEvent_queue;
}

ALLEGRO_TIMER* CAllegroEngine::getTimer() const {
    return this->mTimer;
}

HWND CAllegroEngine::getProcessForegroundWindow() const {
    return this->mProcessForegroundWindow;
}

//  Setters
void CAllegroEngine::setDisplay(ALLEGRO_DISPLAY* lobster) {
    this->mDisplay = lobster;
}

//  Methods
void CAllegroEngine::loadEngine() {

    //  FullScreen settings
    ALLEGRO_DISPLAY_MODE mode;
    al_get_display_mode(al_get_num_display_modes() - 1, &mode);
    al_set_new_display_flags(ALLEGRO_FULLSCREEN);
    
    this->mDisplay = al_create_display(mode.width, mode.height);
  //this->mDisplay = al_create_display(cDisplayWidth, cDisplayHeight);
    assert(this->mDisplay);
    this->mTimer = al_create_timer(deltaT);
    assert(this->mTimer);
    al_start_timer(this->mTimer);

    this->mEvent_queue = al_create_event_queue();
    assert(this->mEvent_queue);
    al_register_event_source(mEvent_queue, al_get_display_event_source(this->mDisplay));
    al_register_event_source(mEvent_queue, al_get_timer_event_source(this->mTimer));
    al_register_event_source(mEvent_queue, al_get_mouse_event_source());  
    al_register_event_source(mEvent_queue, al_get_keyboard_event_source());

    this->loadResources();
}

void CAllegroEngine::loadResources() {
    ifstream* in = new ifstream(parseToCharArray(cSpritesConfig));
    string imageFileName = "";
    while (*in >> imageFileName) {
        ALLEGRO_BITMAP* loadedBitmap = al_load_bitmap(getImagePath(imageFileName));
        if (loadedBitmap != NULL) {
            mSprites.push_back(loadedBitmap);
            mNames.push_back(imageFileName);
        }
        else
            cerr << "Sprite loading error occured: " << imageFileName << "\n";
    }
}

void CAllegroEngine::destroyEngine() {
    al_destroy_display(mDisplay);
    al_destroy_event_queue(mEvent_queue);
    al_destroy_timer(mTimer);
    for (vector<ALLEGRO_BITMAP*>::iterator it = mSprites.begin(); it != mSprites.end(); ++it) {
        if (*it != NULL)
            al_destroy_bitmap(*it);
    }
    mSprites.clear();
    mNames.clear();
}

ALLEGRO_BITMAP* CAllegroEngine::getSpriteById(int id) {
    if (id < (int) this->mSprites.size()) {
        cerr << "Getting Sprite by wrong id: " << id << "th of " << this->mSprites.size() << "\n";
        return NULL;
    }
    return this->mSprites[id];
}

ALLEGRO_BITMAP* CAllegroEngine::getSpriteByName(string name) {
    assert(mSprites.size() == mNames.size());
    vector<ALLEGRO_BITMAP*>::iterator spritesIt = mSprites.begin();
    for (vector<string>::iterator it = mNames.begin(); it != mNames.end(); ++it, ++spritesIt)
        if (name == *it)
            return *spritesIt;
    cerr << "No sprite named \'" << name << "\' has been loaded.\n";
    ALLEGRO_BITMAP* newBitmap = al_load_bitmap(getImagePath(name));
    mSprites.push_back(newBitmap);
    mNames.push_back(name);
    return newBitmap;
}

void CAllegroEngine::setSpriteByName(string name, ALLEGRO_BITMAP* pBmp) {
    assert(mSprites.size() == mNames.size());
    vector<ALLEGRO_BITMAP*>::iterator spritesIt = mSprites.begin();
    for (vector<string>::iterator it = mNames.begin(); it != mNames.end(); ++it, ++spritesIt)
        if (name == *it) {
            *spritesIt = pBmp;
            return;
        }
    cerr << "Nothing to set for \'" << name << "\'.\n";
}

#endif