/**
 * File: ConstDeclarations.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 08.09.12 at 1:24:19.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _CONSTANTS_HEADERS
#define _CONSTANTS_HEADERS

#include "includes.h"

//  Types Declarations
typedef double mass_t;
typedef double coord_t;
typedef double force_t;
typedef pair <coord_t, coord_t> point_t;
typedef pair <coord_t, coord_t> vector_t;
typedef int topology_t;

//  Functions prototypes. Implemented and used further.
string parseToString(double);
char* parseToCharArray(string);
char* getImagePath(string);
size_t GetAppMemUsage();
void drawCentre(ALLEGRO_BITMAP*, coord_t, coord_t);

//  File -related Constants

//  Config files
const string cSpritesConfig         = "PlanetsSprites.conf";

//  Images
const string cImgDirectory          = "res/";
const string cImgDefaultObject      = "4sun.png";//"brick.png";//"planet.png";
const string cImgBackground         = "andromeda.bmp";
const string cImgPlanet             = "planet.png";//"ic_launcher.png";//"Refrigerator.png";
const string cImgSun                = "sun-icon.png";
const string cImgSatellite          = "ic_launcher.png";
const string cImgAppIcon            = "1.ico";
const string cImgLobster            = "cartoon-lobster-7.png";
const string cImgSmallIcon          = "ic_launcher.png";

//  Fonts
const string cFontToribash          = "toribash.ttf";

//  System Configuration -related Constants
const size_t cMemoryLimit           = 128;  //  MBytes
const clock_t cRenderingTimeLimit   = 45;   //  milliseconds

const int   FPS                     = 60;
const double deltaT                 = 1.0 / FPS;
const int   cDisplayWidth           = 1280;
const int   cDisplayHeight          = 800;
const int   cDefaltIteraionCount    = 1;

const int   cShiftDecimalPoint      = 3;
const int   cFontSize               = 72;

//  Model-related Constants

//  Physical Constants
const double cGammaGravityConstant  = 10000.0;
const mass_t cDefaultBigMass        = 1000;
const mass_t cDefaultSmallMass      = 10;
const mass_t cDefaultSatelliteMass  = 1;
const vector_t cZeroVectorT         = make_pair(0, 0);
const vector_t cDefaultSpeed        = cZeroVectorT;
const vector_t cDefaultAcceleration = cZeroVectorT;

const coord_t cSpeedLimit           = 800;
const double cSpeedDecreasingFactor = 0.8;
//  used for normalizing speed in case speed > speed limit

const double cEnergyLossFactor      = 0.0;     //  hard debug required

//  Topology type values definition
const topology_t TOPOLOGY_TYPE_NONE = 0;
const topology_t TOPOLOGY_TYPE_THOR = 1;
const topology_t TOPOLOGY_TYPE_REFL = 2;

const topology_t cDefaultTopologyType   = TOPOLOGY_TYPE_NONE;

//  Coordinates Constants
const coord_t cBordedExceedingLimit = 5;   //  pixels
const coord_t cMiddleX              = cDisplayWidth / 2;
const coord_t cMiddleY              = cDisplayHeight / 2;
const point_t cMiddlePos            = make_pair(cMiddleX, cMiddleY);

const int CLICKED_FLAG_NONE         = -1;

//  Minor constants
const ALLEGRO_EVENT_TYPE NO_EVENT   = 0;

#endif