/**
 * File: CSceneView.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 21.09.12 at 19:43:53.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_VIEW_HEADERS
#define _SCENE_VIEW_HEADERS

#include "ConstDeclarations.h"

class CMassiveObject;
class CPlanetDataContract;
class CPlanetView;

class CSceneView {
    private:
        ALLEGRO_BITMAP* mCanvas;
        ALLEGRO_BITMAP* mBackground;
        
        point_t mMouse;

        vector <CPlanetView*> mList;

    public:
    //  Constructors
        CSceneView();
        CSceneView(ALLEGRO_BITMAP*);

    //  Destructors
        ~CSceneView();

    //  Getters
        ALLEGRO_BITMAP* getBackground() const;
        CPlanetView* getElementAt(int) const;

    //  Setters
        void updateMousePos(point_t);

    //  Methods
        void onUpdateList(vector<CPlanetDataContract*>);

        void drawOne(CMassiveObject*) const;
        void render() const;

        void drawCursor() const;

        ALLEGRO_EVENT receiveEvent();

};

#endif