/**
 * File: includes.h
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 08.09.12 at 1:24:25.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _INCLUDES
#define _INCLUDES

#pragma comment (lib, "allegro-5.0.6-monolith-md-debug.lib")
#pragma comment (lib, "psapi.lib")

#define _CRT_SECURE_NO_WARNINGS

#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <map>
#include <bitset>
#include <string>
using namespace std;
#define REP(i,n) for (int i=0, _n=(n)-1; i <= _n; ++i)
#define FOR(i,a,b) for (int i=(a), _b=(b); i <= _b; ++i)
#define X first
#define Y second
#define pb push_back
#define mp make_pair
#define sz(a) ((int) ((a).size()))
const double Pi = acos(-1.0);
const double eps = 1e-10;
const int INF = 1000*1000*1000 + 7;
const double phi = 0.5 + sqrt(1.25);
typedef long long ll;
typedef pair <int, int> pii;
typedef pair <double, double> pdd;

template < typename T > T sqr (T a) { return (a) * (a); }
template < typename T > T abs (T a) { return (a < 0) ? -(a) : (a); }
template < typename T > T gcd (T a, T b) { return (b) ? gcd(b, a % b) : a; }

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>

#include <Psapi.h>


#endif