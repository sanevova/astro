/**
 * File: CTargetBitmapSwitcher.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 23.09.12 at 17:26:18.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _TARGET_BITMAP_SWITCHER_HEADERS
#define _TARGET_BITMAP_SWITCHER_HEADERS

#include "ConstDeclarations.h"

class CTargetBitmapSwitcher {
    private:
        ALLEGRO_BITMAP* mTargetBitmap;
    public:
        CTargetBitmapSwitcher();
        CTargetBitmapSwitcher(ALLEGRO_BITMAP*);

        ~CTargetBitmapSwitcher();
};

#endif