/**
 * File: CSceneView.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 21.09.12 at 19:44:01.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_VIEW_IMPLEMENTATION
#define _SCENE_VIEW_IMPLEMENTATION

#include "CSceneView.h"

#include "CAllegroEngine.h"
#include "CMassiveObject.h"
#include "CPlanetDataContract.h"
#include "CPlanetView.h"
#include "CTargetBitmapSwitcher.h"

//  Constructors
CSceneView::CSceneView() :
    mMouse(cMiddlePos) {

    this->mBackground = al_load_bitmap(getImagePath(cImgBackground));
    this->mCanvas = al_get_backbuffer(CAllegroEngine::getInstaice()->getDisplay());
}

CSceneView::CSceneView(ALLEGRO_BITMAP* pCanvas) {
    this->mBackground = al_load_bitmap(getImagePath(cImgBackground));
    this->mCanvas = pCanvas;
}

//  Destructors
CSceneView::~CSceneView() {
    for (vector<CPlanetView*>::iterator it = mList.begin(); it != mList.end(); ++it)
        delete *it;
}


//  Getters
CPlanetView* CSceneView::getElementAt(int pos) const {
    assert(pos < (int) mList.size());
    return pos < (int) mList.size()
        ?   this->mList[pos]
        :   NULL;
}

ALLEGRO_BITMAP* CSceneView::getBackground() const {
    return this->mBackground;
}

//  Setters
void CSceneView::updateMousePos(point_t pos) {
    this->mMouse = pos;
}

//  Methods
//  Public
void CSceneView::onUpdateList(vector <CPlanetDataContract*> dataList) {
    this->mList.resize(dataList.size());
    vector<CPlanetDataContract*>::iterator dataIt = dataList.begin();
    for (vector<CPlanetView*>::iterator it = mList.begin(); it != mList.end(); ++it, ++ dataIt) {
        delete *it;
        *it = (*dataIt)->parseToPlanetView();
    }
}

void CSceneView::drawOne(CMassiveObject* obj) const {
    CTargetBitmapSwitcher switcher(this->mCanvas);
    obj->draw();
}

void CSceneView::render() const {
    CTargetBitmapSwitcher switcher(this->mCanvas);
    drawCentre(this->mBackground, cMiddleX, cMiddleY);

    for (vector<CPlanetView*>::const_iterator it = mList.begin(); it != mList.end(); ++it)
        drawCentre((*it)->getBitmap(), (*it)->getPos().X, (*it)->getPos().Y);

}

ALLEGRO_EVENT CSceneView::receiveEvent() {
    ALLEGRO_EVENT ev;
    al_wait_for_event(CAllegroEngine::getInstaice()->getEventQueue(), &ev);
    return ev;
}

void CSceneView::drawCursor() const {
    al_draw_circle(this->mMouse.X, this->mMouse.Y, 50, al_map_rgb(0xff, 0xff, 0xff), 20);
    for (int i = 0; i < 300; ++i)
      al_draw_pixel(this->mMouse.X + rand() % 30, this->mMouse.Y + rand() % 40, al_map_rgba_f(1.0, 0.7, 0.0, 0.3));
}

#endif