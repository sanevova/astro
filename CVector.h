/**
 * File: CVector.h
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 08.09.12 at 2:55:33.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _FORCE_HEADERS
#define _FORCE_HEADERS

#include "ConstDeclarations.h"

class CVector {
    private:
    //  Fields
        vector_t mVector;
        coord_t mValue;

    // Methods
        void updValue();
    public:
    //  Constructors
        CVector();
        CVector(const CVector&);
        CVector(vector_t);
        CVector(coord_t, coord_t);
    
    //  Getters
        coord_t getX() const;
        coord_t getY() const;
        coord_t getValue() const;

    //  Setters
        void setX(coord_t);
        void setY(coord_t);

    //  Methods
        void add(CVector);              //  Adds to the object, which the method is called from
        CVector sum(CVector) const;     //  Adds to the new object, returns the new object
        void toLength(coord_t);         //  actions with the current object
        CVector withLength(coord_t) const;    //  creates the new object

};

#endif