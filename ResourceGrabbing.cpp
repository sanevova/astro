/**
 * File: ResourceGrabbing.cpp
 * Project: Astro 
 *
 * Created by Vova Mishatkin on 08.09.12 at 19:46:40.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _RESOURSE_GRABBING_FUNCTIONS
#define _RESOURSE_GRABBING_FUNCTIONS

#include "ConstDeclarations.h"

string parseToString (double toParce) {
    toParce *= pow(10.0, cShiftDecimalPoint);
    string ret = "";
    int step = 0;
    int num = (int) toParce;
    while (num > 0 || step < cShiftDecimalPoint) {
        ret = ((char) ('0' + (num % 10))) + ret;
        num /= 10;
        ++step;
    }

    ret = ret.substr(0, sz(ret) - cShiftDecimalPoint) + '.' + ret.substr(sz(ret) - cShiftDecimalPoint, cShiftDecimalPoint);
    if (sz(ret) && ret[0] == '.')
        ret = '0' + ret;
    return ret;
}

char* parseToCharArray(string s) {
    int n = s.size();
    char* ret = new char[n + 1];    //  1 more for '\0'
    for (int i = 0; i < n; ++i)
        ret[i] = s[i];
    ret[n] = '\0';
    return ret;
}

char* getImagePath(string sFileName) {
    return parseToCharArray(cImgDirectory + sFileName);
}

size_t GetAppMemUsage() {
    PROCESS_MEMORY_COUNTERS pmc;
 
    pmc.cb = sizeof(pmc);
    GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
 
    return pmc.WorkingSetSize;
}

void drawCentre(ALLEGRO_BITMAP* bmp, coord_t x, coord_t y) {
    al_draw_bitmap(bmp, x - al_get_bitmap_width(bmp) / 2, y - al_get_bitmap_height(bmp) / 2, 0);
}

#endif
