/**
 * File: CTargetBitmapSwitcher.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 23.09.12 at 17:26:26.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _TARGET_BITMAP_SWITCHER_IMPLEMENTATION
#define _TARGET_BITMAP_SWITCHER_IMPLEMENTATION

#include "CTargetBitmapSwitcher.h"
#include "CAllegroEngine.h"

CTargetBitmapSwitcher::CTargetBitmapSwitcher() {
    this->mTargetBitmap = al_get_target_bitmap();
    //al_get_backbuffer(CAllegroEngine::getInstaice()->getDisplay());
}

CTargetBitmapSwitcher::CTargetBitmapSwitcher(ALLEGRO_BITMAP* pSwitchTargetTo) {
    this->mTargetBitmap = al_get_target_bitmap();
    al_set_target_bitmap(pSwitchTargetTo);
}

CTargetBitmapSwitcher::~CTargetBitmapSwitcher() {
    al_set_target_bitmap(this->mTargetBitmap);
}

#endif