/**
 * File: CPlanetDataContact.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 30.09.12 at 23:36:50.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _PLANET_DATA_CONTRACT_HEADERS
#define _PLANET_DATA_CONTRACT_HEADERS

#include "ConstDeclarations.h"

#include "CVector.h"

class CMassiveObject;
class CPlanetView;

class CPlanetDataContract {
//  Successfully copy-pasted from CMAssiveObject
    private:
    //  Fields
    //  Physical parameters
        mass_t mMass;
        point_t mPos;
        CVector mSpeed;
        CVector mAcceleration;
        CVector mForce;

    //  View parameters
        string mImage;
        ALLEGRO_BITMAP* mBitmap;

        coord_t mBitmapRadius;          //  Consider the object as round shaped.
        double mLinearScale;
        double mAngle;
        topology_t mTopology;
        bool mClickedFlag;
        
    public:
    //  Constructors
        CPlanetDataContract(CMassiveObject*);

    //  Destructors
        ~CPlanetDataContract();

    //  Getters
        mass_t getMass() const;
        point_t getPos() const;
        CVector getSpeed() const;
        CVector getAcceleration() const;
        CVector getForce() const;
        string getImage() const;

        ALLEGRO_BITMAP* getBitmap() const;
        coord_t getBitmapRadius() const;
        double getLinearScale() const;
        double getAngle() const;
        topology_t getTopology() const;

        bool isMouseDown() const;

    //  Methods
        CMassiveObject* parseToMassiveObject() const;
        CPlanetView* parseToPlanetView() const;

        void modifyFromView(CPlanetView*);

};

#endif