/**
 * File: CPlanetDataContact.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 30.09.12 at 23:36:57.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _PLANET_DATA_CONTRACT_IMPLEMENTATION
#define _PLANET_DATA_CONTRACT_IMPLEMENTATION

#include "CPlanetDataContract.h"

#include "CMassiveObject.h"
#include "CPlanetView.h"

//  Constructors
CPlanetDataContract::CPlanetDataContract(CMassiveObject* obj) :
    mMass(obj->getMass()),
    mPos(obj->getPos()),
    mSpeed(obj->getSpeed()),
    mAcceleration(obj->getAcceleration()),
    mForce(obj->getForce()),
    mImage(obj->getImage()),
    mBitmapRadius(obj->getBitmapRadius()),
    mLinearScale(obj->getLinearScale()),
    mAngle(obj->getAngle()),
    mTopology(obj->getTopology()),
    mClickedFlag(obj->isMouseDown()) {
        this->mBitmap = obj->getBitmap();
}

CPlanetDataContract::~CPlanetDataContract() {
}

//  Getters
mass_t CPlanetDataContract::getMass() const {
    return this->mMass;
}

point_t CPlanetDataContract::getPos() const {
    return this->mPos;
}

CVector CPlanetDataContract::getSpeed() const {
    return CVector(this->mSpeed);
}

CVector CPlanetDataContract::getAcceleration() const {
    return CVector(this->mAcceleration);
}

CVector CPlanetDataContract::getForce() const {
    return CVector(this->mForce);
}

string CPlanetDataContract::getImage() const {
    return this->mImage;
}

ALLEGRO_BITMAP* CPlanetDataContract::getBitmap() const {
    return this->mBitmap;
}

coord_t CPlanetDataContract::getBitmapRadius() const {
    return this->mBitmapRadius;
}

double CPlanetDataContract::getLinearScale() const {
    return this->mLinearScale;
}

double CPlanetDataContract::getAngle() const {
    return this->mAngle;
}

topology_t CPlanetDataContract::getTopology() const {
    return this->mTopology;   
}

bool CPlanetDataContract::isMouseDown() const {
    return this->mClickedFlag;
}

//  Methods
CMassiveObject* CPlanetDataContract::parseToMassiveObject() const {
    CMassiveObject* newObj = new CMassiveObject(this->mImage, this->mMass, this->mPos, make_pair(this->mSpeed.getX(), this->mSpeed.getY()));
    newObj->setAcceleration(this->mAcceleration);
    newObj->setForce(this->mForce);
    newObj->setTopology(this->mTopology);
    newObj->setClickedFlag(this->mClickedFlag);
    newObj->setBitmap(this->mBitmap);
    return newObj;
}

CPlanetView* CPlanetDataContract::parseToPlanetView() const {
    CPlanetView* newView = new CPlanetView();
    newView->setPos(this->mPos);
    newView->setBitmap(this->mBitmap);
    newView->setClickedFlag(this->mClickedFlag);
    newView->setAngle(this->mAngle);
    return newView;
}

void CPlanetDataContract::modifyFromView(CPlanetView* planetView) {
    this->mPos = planetView->getPos();
    this->mBitmap = planetView->getBitmap();
    this->mAngle = planetView->getAngle();
    this->mClickedFlag = planetView->isMouseDown();
}

#endif