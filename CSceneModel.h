/**
 * File: CSceneModel.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 13.09.12 at 20:25:01.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_MODEL_HEADERS
#define _SCENE_MODEL_HEADERS

#include "ConstDeclarations.h"

class CMassiveObject;
class CVector;
class CPlanetDataContract;
class CSingletonFactory;

class CSceneModel {
    friend class CMassiveObject;
    private:
        vector <CMassiveObject*> mList;
        vector <CVector*> mPrevSpeed;

        struct comparator;
    //  Methods
        void orderListByMass();
        void resizePrev();
    public:
    //  Constructors
        CSceneModel();

    //  Desturctors
        ~CSceneModel();

    //  Getters
        int getListSize();
        CMassiveObject* getElementAt(int) const;

    //  Methods
        void createScene(CSingletonFactory*);
        void createSampleScene(CSingletonFactory*);
        void createFirstScene(CSingletonFactory*);
        void destroyScene();

        void onUpdateList(vector<CPlanetDataContract*>);
        void resetPrevSpeedAt(int);
        
        void performStep();

        void loadFromFile(const char*);
        void saveToFile(const char*) const;
};

#endif