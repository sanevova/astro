/**
 * File: CSceneModel.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 13.09.12 at 20:24:48.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_MODEL_IMPLEMENTATION
#define _SCENE_MODEL_IMPLEMENTATION

#include "CSceneModel.h"

#include "CAllegroEngine.h"
#include "CSingletonFactory.h"
#include "CMassiveObject.h"
#include "CPlanetDataContract.h"

//  Constructors
CSceneModel::CSceneModel() {
    this->mList.resize(0);
    this->mPrevSpeed.resize(0);
}

//  Destructors
CSceneModel::~CSceneModel() {
    this->destroyScene();
}

//  Getters
int CSceneModel::getListSize() {
    return (int) this->mList.size();
}

CMassiveObject* CSceneModel::getElementAt(int pos) const {
    assert(pos < (int) mList.size());
    return pos < (int) mList.size()
        ?   this->mList[pos]
        :   NULL;
}

//  Methods
void CSceneModel::createScene(CSingletonFactory* the_factory) {
    mList.push_back(the_factory->CreatePlanet());
    mList.push_back(the_factory->CreateStar());

    this->orderListByMass();

    this->mList[1]->setPos(make_pair(cDisplayWidth*3/4, cDisplayHeight/2));
    this->mList[1]->throwCounterClockWiseCircleAround(this->mList[0]);

    this->resizePrev();
}

void CSceneModel::createSampleScene(CSingletonFactory* the_factory) {
    mList.push_back(the_factory->CreatePlanet());
    mList.push_back(the_factory->CreateStar());
    mList.push_back(the_factory->CreatePlanet());
    mList.push_back(the_factory->CreateSatellite());

    this->orderListByMass();
    this->mList[0]->setMass(cDefaultSmallMass - 5);
    this->mList[0]->scaleAndRotate(0.3, 0);

    this->mList[1]->setPos(make_pair(cDisplayWidth/4, cDisplayHeight/2));
    this->mList[2]->setPos(make_pair(cDisplayWidth*3/4, cDisplayHeight/2));

    this->mList[1]->throwClockWiseCircleAround(mList[2]);
    this->mList[2]->throwClockWiseCircleAround(mList[0]);
    this->mList[1]->setSpeed(this->mList[1]->getSpeed().withLength(this->mList[1]->getSpeed().getValue() * 10));
    this->mList[2]->setSpeed(mp(this->mList[1]->getSpeed().getY(), -1.0*this->mList[1]->getSpeed().getY()));
    
    this->mList[3]->setPos(make_pair(cDisplayWidth/4 - 50, cMiddleY));
    this->mList[3]->throwCounterClockWiseCircleAround(mList[0]);
    this->mList[3]->scaleAndRotate(0.8, 120);
    this->orderListByMass();
    this->mList[2]->setMass(cDefaultBigMass);
    this->mList[3]->throwCounterClockWiseCircleAround(this->mList[2]);
    this->orderListByMass();
    
    this->resizePrev();
}

void CSceneModel::createFirstScene(CSingletonFactory* the_factory) {
    this->mList.push_back(the_factory->CreateStar());
    this->mList.push_back(the_factory->CreatePlanet());
    this->mList.push_back(the_factory->CreateSatellite());
    this->orderListByMass();

    this->mList[0]->setPos(make_pair(cDisplayWidth / 4, cMiddleY));
    this->mList[0]->scaleAndRotate(0.4, 120);
    this->mList[0]->setMass(250);
    this->mList[0]->setPos(make_pair(cMiddleX, cMiddleY));

    this->mList[1]->setPos(make_pair(cDisplayWidth * 3 / 4, cMiddleY));
//    this->mList[1]->setSpeed(CVector(0, -50));
    this->mList[1]->throwClockWiseCircleAround(this->mList[0]);
    this->mList[1]->scaleAndRotate(0.8, 0);

    this->mList[2]->setPos(make_pair(this->mList[1]->getPos().X + 50, cMiddleY));
    this->mList[2]->throwCounterClockWiseCircleAround(this->mList[1]);
    this->mList[2]->setSpeed(this->mList[2]->getSpeed().sum(this->mList[1]->getSpeed()));
    this->mList[2]->scaleAndRotate(0.9, -30);
    this->mList[2]->setMass(this->mList[1]->getMass() * 0.2);
    for (vector<CMassiveObject*>::iterator it = mList.begin(); it != mList.end(); ++it)
        (*it)->setTopology(TOPOLOGY_TYPE_NONE);
    this->mList[0]->setTopology(TOPOLOGY_TYPE_REFL);

    this->resizePrev();
}

void CSceneModel::destroyScene() {
    for (vector<CMassiveObject*>::iterator it = mList.begin(); it != mList.end(); ++it)
        delete *it;
    mList.resize(0);
}

void CSceneModel::onUpdateList(vector<CPlanetDataContract*> dataList) {
    this->mList.resize(dataList.size());
    vector<CPlanetDataContract*>::iterator dataIt = dataList.begin();
    for (vector<CMassiveObject*>::iterator it = mList.begin(); it != mList.end(); ++it) {
        delete *it;
        *it = (*dataIt)->parseToMassiveObject();
    }
}

void CSceneModel::resetPrevSpeedAt(int id) {
    assert(id < (int) this->mPrevSpeed.size());
    delete this->mPrevSpeed[id];
    this->mPrevSpeed[id] = new CVector();
}

void CSceneModel::performStep() {
    int n = mList.size();
    CVector newForce(cZeroVectorT);
    for (vector<CMassiveObject*>::iterator i = mList.begin(); i != mList.end(); ++i) {
        newForce.setX(0);
        newForce.setY(0);
        for (vector<CMassiveObject*>::iterator j = mList.begin(); j != mList.end(); ++j)
            if (i != j)
                newForce.add((*i)->getForceBetween(*j));
        (*i)->setForce(newForce);
    }

    //  Runge-Kuth method.
    vector<CMassiveObject*>::iterator listIt = mList.begin();
    for (vector<CVector*>::iterator speedIt = mPrevSpeed.begin();
         speedIt != mPrevSpeed.end() && listIt != mList.end(); ++speedIt, ++listIt)

        if (abs((*speedIt)->getX() - (*listIt)->getSpeed().getX()) > 0 &&
            abs((*speedIt)->getY() - (*listIt)->getSpeed().getY()) > 0 &&
            (*speedIt)->getValue() > eps) {

            CVector newSpeed = (*listIt)->getSpeed().sum(*(*speedIt));
            (*listIt)->setSpeed(newSpeed);

            (*speedIt)->setX((*listIt)->getSpeed().getX());
            (*speedIt)->setY((*listIt)->getSpeed().getY());
        }


    for (vector<CMassiveObject*>::iterator it = mList.begin(); it != mList.end(); ++it) {
        (*it)->move();
    }

}

void CSceneModel::saveToFile(const char* fileName) const {

}

//  Private
struct CSceneModel::comparator {
    bool operator () (CMassiveObject* a, CMassiveObject* b) {
        return a->getMass() > b->getMass();
    }
} massComp;

void CSceneModel::orderListByMass() {
    sort(mList.begin(), mList.end(), massComp);
}

void CSceneModel::resizePrev() {
    this->mPrevSpeed.resize(this->mList.size());
    for (vector<CVector*>::iterator it = mPrevSpeed.begin(); it != mPrevSpeed.end(); ++it)
        *it = new CVector();
}

#endif