/**
 * File: CSceneController.h
 * Project: Astro
 *
 * Created by Vova Mishatkin on 21.09.12 at 19:38:25.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

#ifndef _SCENE_CONTROLLER_HEADERS
#define _SCENE_CONTROLLER_HEADERS

#include "ConstDeclarations.h"

class CSceneModel;
class CSceneView;
class CMassiveObject;
class CPlanetDataContract;

class CSceneController {
    private:
        CSceneModel* mModel;
        CSceneView* mView;

        vector <CPlanetDataContract*> mDataCash;

        int mIterationsPerStep;

        ALLEGRO_EVENT mEventHandler;
        bool mMainLoopContinue;

    //  Methods
        void modifyModel();
        void passToDrawer(CMassiveObject*);
        void selfUpdateFromModel();
        bool updEvent();
    public:
    //  Constructors
        CSceneController();
        CSceneController(CSceneModel*, CSceneView*);

    //  Destructors
        ~CSceneController();

    //  Getters
        ALLEGRO_EVENT getCurrentEvent() const;
        CSceneModel* getModel() const;
        CSceneView* getView() const;
        int getIterationsPerStep() const;

    //  Setters
        void setCurrentEvent(ALLEGRO_EVENT);
        void setIterationsPerStep(int);

    //  Methods
        void performLoopStep();

};

#endif