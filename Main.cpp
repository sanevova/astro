/**
 * File: Main.cpp
 * Project: Astro
 *
 * Created by Vova Mishatkin on 07.09.12 at 21:28:15.
 * Copyright � 2012 LobsterSolutions Inc. All rights reserved.
 */

//Linker Additional Dependencies settings -     allegro-5.0.6-monolith-md-debug.lib;%(AdditionalDependencies)

#include "CWorldController.h"

int main() {
    freopen("logFile.log", "w", stderr);
    CWorldController* world = new CWorldController();
    world->launch();
    delete world;
    return EXIT_SUCCESS;
}